package com.example.calculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView display,textDel,texteql,
            text1,text2,text3,text4,text5,text6,text7,text8,text9,
            text0,textdiv,textadd,textsub,textmul,textRemainder,textDot,decimal;
    double input1 = 0, input2 = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        text1 = (TextView) findViewById(R.id.txt1);
        text2 = (TextView) findViewById(R.id.txt2);
        text3 = (TextView) findViewById(R.id.txt3);
        text4 = (TextView) findViewById(R.id.txt4);
        text5 = (TextView) findViewById(R.id.txt5);
        text6 = (TextView) findViewById(R.id.txt6);
        text7 = (TextView) findViewById(R.id.txt7);
        text8 = (TextView) findViewById(R.id.txt8);
        text9 = (TextView) findViewById(R.id.txt9);
        text0 = (TextView) findViewById(R.id.txt0);
        textadd = (TextView) findViewById(R.id.txtadd);
        textsub = (TextView) findViewById(R.id.txtsub);
        textmul = (TextView) findViewById(R.id.txtdiv);
        textdiv = (TextView) findViewById(R.id.txtdiv);
        textDel = (TextView) findViewById(R.id.txtDel);
        texteql = (TextView) findViewById(R.id.txteql);
        display = (TextView) findViewById(R.id.display);

        text1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display.setText(display.getText() + "1");
            }
        });

        text2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display.setText(display.getText() + "2");
            }
        });

        text3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display.setText(display.getText() + "3");
            }
        });

        text4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display.setText(display.getText() + "4");
            }
        });

        text5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display.setText(display.getText() + "5");
            }
        });

        text6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display.setText(display.getText() + "6");
            }
        });

        text7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display.setText(display.getText() + "7");
            }
        });

        text8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display.setText(display.getText() + "8");
            }
        });

        text9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display.setText(display.getText() + "9");
            }
        });

        text0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                display.setText(display.getText() + "0");
            }
        });

        textadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    display.setText(display.getText() + "+");
                }
        });

        textsub.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                display.setText(display.getText() + "-");
            }
        });

        textmul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                display.setText(display.getText() + "*");
            }
        });

        textdiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                display.setText(display.getText() + "/");
            }
        });

        textDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                display.setText(display.getText() + "Del");
            }
        });

        texteql.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                display.setText(display.getText() + "=");
            }
        });








    }
}